import requests
from bs4 import BeautifulSoup
import json
import re

article_class = "corps-article"

games = None
with open("games.json", "r") as file_in:
    games = json.load(file_in)

game_link_exp = re.compile(r"^\/jeux\/(?:jeu-\d+\/|.+\/\d+-.*\.htm)$")
game_names_and_links = {}
out = {}
try:
    with open("gamereviews.json", "r") as file_in:
        out = json.load(file_in)
except FileNotFoundError as e:
    out = {} 

try:
    with open("gamelinks.json", "r") as file_in:
        game_names_and_links = json.load(file_in)
except FileNotFoundError as e:
    pass

changed = False
for page in games.keys():
    print(page)
    for game in games[page]:
        if game["game"] not in out:
            review = requests.get(game["review_url"])
            review = BeautifulSoup(review.text, features="html.parser")
            links = review.find_all(href=game_link_exp)
            if len(links) > 0:
                main_link_tag = links[0]
                link = main_link_tag["href"]
                link = "https://www.jeuxvideo.com" + link
                game_name = main_link_tag.text
            else:
                link = game["game_link"]
                game_name = game["game"]
            txt = review.find(id="article-jv").find_all("p")
            #print(review.prettify())
            txt = [t.text for t in txt]
            full = "\n".join(txt)
            game_names_and_links[game_name] = {"link": link}
            out[game_name] = {"text":full}
            changed = True

    print("Writing to file…")
    if changed:
        with open("gamereviews.json", "w") as file_out:
            json.dump(out, file_out, indent=2)

        with open("gamelinks.json", "w") as file_out:
            json.dump(game_names_and_links, file_out, indent=2)
        changed = False
    print("Done")
