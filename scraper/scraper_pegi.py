import requests
from bs4 import BeautifulSoup
import json

games = {}
with open("gamelinks.json", "r") as file_in:
    games = json.load(file_in)

ratings = {}
try:
    with open("gameratings.json", "r") as file_in:
        ratings = json.load(file_in)
except FileNotFoundError as e:
    pass

print(ratings)
counter = 0
for game in games:
    print(game)
    link = games[game]["link"]
    if game not in ratings:
        game_page = BeautifulSoup(requests.get(link).text, features="html.parser")
        rows = game_page.find_all(class_="gameCharacteristicsDetailed__tr")
        classif = "None"
        for row in rows:
            if "Classification" in row.div.text:
                classif = row.span.text.strip()

        ratings[game] = classif
    
    

    counter += 1
    if counter % 10 == 0:
        print("writing to file…")
        with open("gameratings.json", "w") as file_out:
            json.dump(ratings, file_out)
        print("Done")
        print(f"counter: {counter}")

with open("gameratings.json", "w") as file_out:
    json.dump(ratings, file_out)
