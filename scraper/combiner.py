import json

game_reviews = {}
with open("gamereviews.json", "r") as file_in:
    game_reviews = json.load(file_in)

game_ratings = {}
with open("gameratings.json", "r") as file_in:
    game_ratings = json.load(file_in)

out = []

rating_strings = ["None", "+3 ans", "+4 ans", "+7 ans", "+9 ans", "+12 ans","+16 ans", "+17 ans", "+18 ans"]
for game in game_reviews:
    rating = game_ratings[game]
    rating = rating_strings.index(rating)
    review = game_reviews[game]["text"]
    out.append({"game": game, "review": review, "rating": rating})

with open("gamesratingsreviews.json", "w") as file_out:
    json.dump(out, file_out)

