import requests
from bs4 import BeautifulSoup
import json

games = {}
for i in range(1,917):
    print(i)
    games[i] = []
    if i == 1:
        resp = requests.get(f"https://www.jeuxvideo.com/tests.htm")
    else:
        resp = requests.get(f"https://www.jeuxvideo.com/tests/?p={i}")

    soup = BeautifulSoup(resp.text, features="html.parser").ol.find_all("li")
    for s in soup:
        game_name = s.select("div > a")
        if len(game_name) > 0:
            game_name = game_name[0]
            game_link = game_name["href"]
            game_name = game_name.text
            #print(game_name)
            review_link = s.select("h2 > a")
            if len(review_link) > 0:
                review_link = review_link[0]["href"]
                #print(f"https://www.jeuxvideo.com{review_link}")
                game = {"game": game_name, "game_link": f"https://www.jeuxvideo.com{game_link}",
                        "review_url": f"https://www.jeuxvideo.com{review_link}"}
                games[i].append(game)

with open("games.json", "w") as out_file:
    json.dump(games, out_file, indent = 2)
