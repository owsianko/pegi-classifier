import torch
import torch.nn as nn
import torch.nn.functional as F

class simpleNetworkClassifier(nn.Module):

    def __init__(self, input_dim, output_dim, hidden_dim=64):
        '''
            Setup a network Module.
            
            Args:
                input_dim (int) number of input nodes
                hidden_dim (int) number of hidden nodes
                output_dim (int) number of output nodes
        '''
        super(simpleNetworkClassifier, self).__init__()
        self.fc1 = nn.Linear(in_features = D, out_features = 256)
        self.hidden1 = nn.Linear(in_features = 256, out_features = 128)
        self.hidden2 = nn.Linear(in_features = 128, out_features = H)
        self.fc2 = nn.Linear(in_features = H, out_features = L)

    def forward(self, x):
        '''
            Defines the forward pass of the network. 
            
            Args:
                x : Tensor of input data
                
            Returns:
                y : Tensor of output data
        '''
        x = F.relu(self.fc1(x))
        x = F.relu(self.hidden1(x.view(-1)))
        z = F.relu(self.hidden2(x.view(-1)))
        y = F.relu(self.fc(z.view(-1)))
        return torch.softmax(y)
            