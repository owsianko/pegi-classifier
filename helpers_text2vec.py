from gensim.models.doc2vec import TaggedDocument
import numpy as np
import nltk
from nltk.tokenize import word_tokenize
from nltk.stem.snowball import FrenchStemmer

def remove_unusable_files(texts_array, titles_array, pegis):
    """Removes the files with the age unknown, or with ages 4, 9, or 17

    Args:
        texts_array ([type]): [description]
        titles_array ([type]): [description]
        pegis ([type]): [description]
    """
    texts_array = np.array(texts_array)
    titles_array = np.array(titles_array)
    pegis = np.array(pegis)
    pegis_to_drop = [0, 2, 4, 7]
    indices_to_keep = [x not in pegis_to_drop for x in pegis]
    texts_array, titles_array, pegis = texts_array[indices_to_keep], titles_array[indices_to_keep], pegis[indices_to_keep]
    return texts_array, titles_array, pegis

def getStopWords(corpus, n_words = 100):
    freq_totale = nltk.Counter()
    freq = {}
    token_texts = [word_tokenize(d.lower(), language='french') for d in corpus]
    for k, v in enumerate(token_texts):
        freq[k] = nltk.FreqDist(v)
        freq_totale += freq[k]
    sort_orders = sorted(freq_totale.items(), key=lambda x: x[1], reverse=True)
    if(n_words > len(sort_orders)):
        n_words = len(sort_orders)
        print("All words selected as stop words...")
    words = [item[0] for item in sort_orders[:n_words]]
    return words

def prepare_tagged_bags(corpus, language = 'french', stemming = False, use_stopwords = False, stopwords = []):
    bag = []
    if (stemming is True):
        stemmer = FrenchStemmer(ignore_stopwords=True)
    if(use_stopwords is True):
        stops = set()
        # Add classical stopwords
        stops.update(tuple(nltk.corpus.stopwords.words('french')))
        # Add the inputed stopwords
        stops.update(stopwords)
    for d in corpus:
        bow = word_tokenize(d.lower(), language='french')
        if (stemming is True):
            if (use_stopwords is True):
                bow = [stemmer.stem(x) for x in bow if x not in stops]
            else:
                bow = [stemmer.stem(x) for x in bow]
        else:
            if(stopwords is True):
                bow = [x for x in bow if x not in stops]
        bag.append(bow)
    tagged_bags = [TaggedDocument(doc, [i]) for i, doc in enumerate(bag)]
    return tagged_bags

def predict_neighbors(tagged_bag, model, k = 5):
    predicted_neighbors = []
    for test_doc in tagged_bag:
        predicted_neighbors.append([doc[0] for doc in model.docvecs.most_similar(positive=[model.infer_vector(test_doc[0])],topn=k)])
    return predicted_neighbors

def predict_pegis(predicted_neighbors, train_pegis):
    predicted_pegis = []
    for neighbors in predicted_neighbors:
        predicted_pegis.append(predict_pegi(neighbors, train_pegis))
    return predicted_pegis

def predict_pegi(predicted_neighbors, train_pegis):
    """For a single text"""
    pegis = np.array(train_pegis[predicted_neighbors])
    counts = np.bincount(np.array(train_pegis[predicted_neighbors]))
    # Just the argmax, not very good  method but used for the first test.
    return np.argmax(counts)