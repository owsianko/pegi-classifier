from pickle import Unpickler
import torch
import numpy as np

def load_raw(path, subsample):
    data = []
    counter = 1
    with open(path, "rb") as input_file:
        unpickler = Unpickler(input_file)
        try:
            while True:
                d = unpickler.load()
                if (not subsample) or counter % 20 == 0:
                    data.append(d)
                counter += 1
        except EOFError:
            pass
    
    return data

def load_compressed_data(location, vector_path, label_path, title_path, batch_size = 100, subsample = False):
    vector_data = load_raw(location + vector_path, subsample)
    label_data = load_raw(location + label_path,   subsample)
    title_data = load_raw(location + title_path,   subsample)
    assert len(vector_data) == len(label_data)
    assert len(label_data) == len(title_data)
    num_features = len(vector_data[0][0])
    label_mapping = [1, 3, 5, 6, 8]
    vectors, labels, titles = [], [], []
    for vector, label, title in zip(vector_data[0], label_data[0], title_data[0]):
        if label in label_mapping:
            new_label = label_mapping.index(label)
            vectors.append(vector)
            labels.append(new_label)
            titles.append(title)

    data = torch.utils.data.DataLoader(list(zip(vectors, labels, titles)))
    num_labels = len(label_mapping)
    return data, num_features, num_labels

def load_stemmed(location, vector_path, test_proportion = 0.25, batch_size = 100, subsample = False, split = True):
    data = load_raw(location + vector_path, subsample)
    num_test_data = int(test_proportion * len(data))
    label_mapping = [1, 3, 5, 6, 8]
    num_features = len(data[0]["text_vector"][0])

    if split:
        test_data, train_data = torch.utils.data.random_split(data, [num_test_data, len(data)-num_test_data])
        test_vectors, test_titles, test_ratings = to_tuple(test_data, label_mapping)
        train_vectors, train_titles, train_ratings = to_tuple(train_data, label_mapping)
        train_data = torch.utils.data.DataLoader(list(zip(train_vectors, train_ratings, train_titles)), batch_size=batch_size)
        test_data = torch.utils.data.DataLoader(list(zip(test_vectors, test_ratings, test_titles)), batch_size=batch_size)
        return test_data, train_data, num_features, len(label_mapping)
    else:
        data = to_tuple(data, label_mapping)
        return torch.utils.data.DataLoader(data, batch_size=batch_size), num_features, len(label_mapping)
        
    

def to_tuple(data, label_mapping):
    vectors, titles, ratings = [], [], []
    for d in data:
        rating = d["rating"]
        if rating in label_mapping:
            new_rating = label_mapping.index(rating)
            vector = d["text_vector"][0]
            vectors.append(vector)
            titles.append(d["game"])
            ratings.append(new_rating)

    return vectors, titles, ratings


def load_large_data(location, vector_path, test_proportion, batch_size = 100, subsample = False, split = True):
    data = load_raw(location + vector_path, subsample)
    num_test_data = int(test_proportion * len(data))
    label_mapping = [1, 3, 5, 6, 8]
    num_features = len(data[0]["text_vector"][0])

    if split:
        test_data, train_data = torch.utils.data.random_split(data, [num_test_data, len(data)-num_test_data])
        test_vectors, test_titles, test_ratings = to_tuple(test_data, label_mapping)
        train_vectors, train_titles, train_ratings = to_tuple(train_data, label_mapping)
        train_data = torch.utils.data.DataLoader(list(zip(train_vectors, train_ratings, train_titles)), batch_size=batch_size)
        test_data = torch.utils.data.DataLoader(list(zip(test_vectors, test_ratings, test_titles)), batch_size=batch_size)
        del data
        return test_data, train_data, num_features, len(label_mapping)
    else:
        data = to_tuple(data, label_mapping)
        del data
        return torch.utils.data.DataLoader(data, batch_size=batch_size), num_features, len(label_mapping)

def train(model, criterion, dataset_train, dataset_test, optimizer, num_epochs, device):
    """
    @param model: torch.nn.Module
    @param criterion: torch.nn.modules.loss._Loss
    @param dataset_train: torch.utils.data.DataLoader
    @param dataset_test: torch.utils.data.DataLoader
    @param optimizer: torch.optim.Optimizer
    @param num_epochs: int
    """
    print("Starting training")
    for epoch in range(num_epochs):
        # Train an epoch
        model.train()
        for batch_x, batch_y, *rest in dataset_train:
            batch_x, batch_y = batch_x.to(device), batch_y.to(device)

            pred = model(batch_x)
            loss = criterion(pred, batch_y)
            # Compute the gradient
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        model.eval()
        accuracies_test = []
        tol_acc = []
        losses_test = []
        for batch_x, batch_y, *rest in dataset_test:
            batch_x, batch_y = batch_x.to(device), batch_y.to(device)

            # Evaluate the network (forward pass)
            prediction = model(batch_x)
            accuracies_test.append(accuracy(prediction, batch_y))
            tol_acc.append(tolerant_accuracy(prediction, batch_y))
            losses_test.append(criterion(prediction, batch_y))

        print("Epoch {} | Test accuracy: {:.5f} | Test loss: {:.5f} | Test soft accuracy: {:.5f}"\
            .format(epoch, sum(accuracies_test).item()/len(accuracies_test),
            sum(losses_test).item()/len(losses_test),
            sum(tol_acc).item()    /len(tol_acc)))

def accuracy(predicted_logits, reference):
    """
    Compute the ratio of correctly predicted labels
    
    @param predicted_logits: float32 tensor of shape (batch size, num classes)
    @param reference: int64 tensor of shape (batch_size) with the class number
    """
    return (predicted_logits.argmax(axis=1)==reference).sum() / len(reference)

def tolerant_accuracy(predicted_logits, reference):
    """
    Compute the ratio of labels within the same colour rating. Pegi +3 and +7,
    +12 and +16, are grouped, while +18 remains its own category.

    @param predicted_logits: float32 tensor of shape (batch size, num classes)
    @param reference: int64 tensor of shape (batch_size) with the class number
    """
    reference[reference==1] = 0
    reference[reference==3] = 2
    predicted = predicted_logits.argmax(axis=1)
    predicted[predicted==1] = 0
    predicted[predicted==3] = 2
    return (predicted == reference).sum() / len(reference)