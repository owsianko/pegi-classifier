"""
Helpers used for the vectorization of the text data with tfidf.
"""
import _pickle as cPickle
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer

JSON_TXT = "review"
JSON_TITLE = "game"
JSON_PEGI = "rating"
JSON_VECTOR = "text_vector"

def data_divide_arrays(data):
    """Divide the json file input data into the categories for conducting our analysis

    Args:
        data (json): json file containing the PEGI, title, and text of reviews

    Returns:
        texts  (list): [list of the texts of the reviews]
        titles (list): [list of the titles of the reviews]
        pegis  (list): [list of the PEGI classification of the reviews] 
    """
    texts = []
    titles = []
    pegis = []
    for elem in data:
        texts.append(elem[JSON_TXT])
        titles.append(elem[JSON_TITLE])
        pegis.append(elem[JSON_PEGI])
    return texts, titles, pegis

def compute_complete_voc_occurences(vectorizer, texts_array):
    """Computes the word occurences over all the texts with the given vectorizer
    Args:
        vectorizer: [vectorizer to use]
        texts_array: [array containing all the texts]
    """
    print(f"computing word occurences over {len(texts_array)} reviews")
    text = ['\n'.join(texts_array)]
    vector = vectorizer.fit_transform(text)
    return vector

def construct_vocabulary(n_grams, stop_words, texts_array):
    """ Computes the vocabulary over all the texts
    Args:
        n_grams: the size of word groups the vectorizer will consider
        stop_words: array of words to ignore
        texts_array: all the reviews on which the vocabulary is to be constructed
    """
    count_vectorizer = CountVectorizer(ngram_range=(n_grams, n_grams), lowercase=True, strip_accents= 'unicode', stop_words=stop_words)
    v = compute_complete_voc_occurences(count_vectorizer, texts_array)
    dic = dict(zip(count_vectorizer.get_feature_names(), np.asarray(v.sum(axis=0)).ravel())) 
    sorted_tuples = sorted(dic.items(), key=lambda item: item[1])
    vocabulary = {k: v for k, v in sorted_tuples}
    return vocabulary

def save_vectorized_dico(titles, sparse_vectors, PEGI, output_path):
    """Saves the dictionary with elements titles, vectors, PEGI under the output_path filename.
    The sparse matrix in entry is converted to array line by line for memory reasons.
    Args:
        titles (list): [List of game names]
        sparse_vectors ([sparse matrix]): [Sparse matrix of tfidf]
        PEGI ([list]): [List of the game PEGIs]
        output_path ([String]): [output file path]
    """
    assert (len(titles) == sparse_vectors.shape[0] and sparse_vectors.shape[0] == len(PEGI))
    keys = [JSON_TITLE, JSON_VECTOR, JSON_PEGI]
    with open(output_path, 'wb') as output_file:
        #to write file line by line
        pickler = cPickle.Pickler(output_file)
        for i in range(len(titles)):
            if (i %1000 == 0):
                print("File {}/{}".format(i, len(titles)))
            dico = dict(zip(keys, [titles[i], sparse_vectors[i].toarray(), PEGI[i]]))
            pickler.dump(dico)

